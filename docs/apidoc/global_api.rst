.. _api:

**********
Global API
**********

Root module
============

.. automodule:: enoslib
    :members: init_logging, check

API module
==========

.. automodule:: enoslib.api
    :members: run_play, play_on, actions, run_command, run, gather_facts, run_ansible, sync_info, generate_inventory, get_hosts, wait_for
